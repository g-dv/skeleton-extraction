# Skeleton Extraction With Mesh Contraction

**Table of Contents**

- [Skeleton Extraction With Mesh Contraction](#skeleton-extraction-with-mesh-contraction)
  - [Notice](#notice)
  - [Abstract](#abstract)
  - [Source](#source)
  - [Demo](#demo)
  - [Dependencies](#dependencies)
  - [Mathematical model](#mathematical-model)
  - [What could be improved?](#what-could-be-improved)
    - [Initial weights](#initial-weights)
    - [Explosive cotangent weights](#explosive-cotangent-weights)
    - [Embedding refinement](#embedding-refinement)
    - [Tools](#tools)
  - [Results analysis](#results-analysis)
    - [Dancer](#dancer)
    - [Fertility](#fertility)
    - [Killeroo](#killeroo)
    - [Knot](#knot)
    - [Octahedron](#octahedron)
    - [Sphere](#sphere)
    - [Polygirl](#polygirl)
    - [Robot](#robot)
    - [Temple](#temple)

## Notice

I'm sorry, the source code for this project is no longer available. However, if you really are interested, feel free to contact me.

## Abstract

This software we made extracts the skeleton of 3D shapes using mesh contraction.

Skeleton extraction is an important aspect of 3D modeling since skeletons are a simplified but faithful representations that can be used for:

 * shape morphing
 * animation
 * merging shapes
 * retrieving shapes

As of today, there are many different methods to extract skeletons, but none is completely satisfying, simple and robust. This is an avenue for future research.

## Source

The method used is described in the following article:

> Au, O. K. C., Tai, C. L., Chu, H. K., Cohen-Or, D., & Lee, T. Y. (2008, August). Skeleton extraction by mesh contraction. In _ACM transactions on graphics (TOG)_ (Vol. 27, No. 3, p. 44). ACM.

The abstract of the article states:

> The method works directly on the mesh domain, without pre-sampling the mesh model into a volumetric representation. The method first contracts the mesh geometry into zero-volume skeletal shape by applying implicit Laplacian smoothing with global positional constraints. The contraction does not alter the mesh connectivity and retains the key features of the original mesh. The contracted mesh is then converted into a 1D curve-skeleton through a connectivity surgery process to remove all the collapsed faces while preserving the shape of the contracted mesh and the original topology.

## Demo

![Demo](results/demo.gif)

## Dependencies

This project uses:

* Qt5
* [Eigen](http://eigen.tuxfamily.org) (Core, SparceCore, SparseCholesky) to solve the different systems as fast as possible
* [libQGLViewer](http://libqglviewer.com/)

## Mathematical model

Please have a look at [these slides](theory.pdf) or read the original article if you need more details.

## What could be improved?

### Initial weights

There is probably a better formula to make the compromise between the speed of collapse and the conservation of the original shape.

### Explosive cotangent weights

Several attempts to solve the problem:
  * Cap the maximum cotangent value
  * Fix vertices of near-zero area triangles
  * Position not updated if the displacement is too high
  * Etc.

### Embedding refinement

Skeletal nodes going outside the mesh / off-centered

* Move skeleton node to approximate center of its corresponding local mesh

### Tools

More tools should be added for the user to use:

* "Stick" vertices to their position
* Manually collapse vertices
* Define areas
* Etc.

## Results analysis

### Dancer

|                                   |                                   |                                   |
| :-------------------------------: | :-------------------------------: | :-------------------------------: |
| ![dancer_1](results/dancer_1.png) | ![dancer_1](results/dancer_2.png) | ![dancer_1](results/dancer_3.png) |
|          1. Initial mesh          |       2. After 1 iteration        |       3. After n iterations       |
| ![dancer_1](results/dancer_4.png) | ![dancer_1](results/dancer_5.png) | ![dancer_1](results/dancer_6.png) |
|         4. After surgery          |        5. After clustering        |  6. Comparison with initial mesh  |

**Analysis:** It's working pretty well: the global shape is well-conserved (the skirt did not get in the way) and the skeleton fits the original mesh (see step 6).
But, we notice that the arms merged.

### Fertility

|                                         |                                         |                                         |
| :-------------------------------------: | :-------------------------------------: | :-------------------------------------: |
| ![fertility_1](results/fertility_1.png) | ![fertility_1](results/fertility_2.png) | ![fertility_1](results/fertility_3.png) |
|             1. Initial mesh             |          2. After 1 iteration           |          3. After n iterations          |
| ![fertility_1](results/fertility_4.png) | ![fertility_1](results/fertility_5.png) | ![fertility_1](results/fertility_6.png) |
|            4. After surgery             |           5. After clustering           |     6. Comparison with initial mesh     |

**Analysis:** This mesh was topologically very complex and the algorithm worked very well, but the Laplacian could not reach an area of 0. Therefore, the skeleton must be cleaned manually afterwards.

### Killeroo

|                                       |                                       |                                       |
| :-----------------------------------: | :-----------------------------------: | :-----------------------------------: |
| ![killeroo_1](results/killeroo_1.png) | ![killeroo_1](results/killeroo_2.png) | ![killeroo_1](results/killeroo_3.png) |
|            1. Initial mesh            |         2. After 1 iteration          |         3. After n iterations         |
| ![killeroo_1](results/killeroo_4.png) | ![killeroo_1](results/killeroo_5.png) | ![killeroo_1](results/killeroo_6.png) |
|           4. After surgery            |          5. After clustering          |    6. Comparison with initial mesh    |

**Analysis:** Except some minor problems linked to the Laplacian that could not reach a 0-area mesh, the algorithm worked very well.

### Knot

|                               |                               |                                 |
| :---------------------------: | :---------------------------: | :-----------------------------: |
| ![knot_1](results/knot_1.png) | ![knot_1](results/knot_2.png) |  ![knot_1](results/knot_3.png)  |
|        1. Initial mesh        |     2. After 1 iteration      |      3. After n iterations      |
| ![knot_1](results/knot_4.png) | ![knot_1](results/knot_5.png) |  ![knot_1](results/knot_6.png)  |
|       4. After surgery        |      5. After clustering      | 6. Comparison with initial mesh |

**Analysis:** The algorithm worked almost perfectly. A minor problem can be seen image 5 (bottom-left).

### Octahedron

|                                           |                                           |                                           |
| :---------------------------------------: | :---------------------------------------: | :---------------------------------------: |
| ![octahedron_1](results/octahedron_1.png) | ![octahedron_1](results/octahedron_2.png) | ![octahedron_1](results/octahedron_3.png) |
|              1. Initial mesh              |           2. After 1 iteration            |           3. After n iterations           |
| ![octahedron_1](results/octahedron_4.png) | ![octahedron_1](results/octahedron_5.png) | ![octahedron_1](results/octahedron_6.png) |
|             4. After surgery              |            5. After clustering            |      6. Comparison with initial mesh      |

**Analysis:** The skeleton was indeed supposed to be a single point (the center of the octahedron). The algorithm worked as expected.

### Sphere

|                                   |                                   |                                   |
| :-------------------------------: | :-------------------------------: | :-------------------------------: |
| ![sphere_1](results/sphere_1.png) | ![sphere_1](results/sphere_2.png) | ![sphere_1](results/sphere_3.png) |
|          1. Initial mesh          |       2. After 1 iteration        |       3. After n iterations       |
| ![sphere_1](results/sphere_4.png) | ![sphere_1](results/sphere_5.png) | ![sphere_1](results/sphere_6.png) |
|         4. After surgery          |        5. After clustering        |  6. Comparison with initial mesh  |

**Analysis:** Same as the one before: The skeleton was indeed supposed to be a single point (the center of the sphere). The algorithm worked as expected.

### Polygirl

|                                       |                                       |                                       |
| :-----------------------------------: | :-----------------------------------: | :-----------------------------------: |
| ![polygirl_1](results/polygirl_1.png) | ![polygirl_1](results/polygirl_2.png) | ![polygirl_1](results/polygirl_3.png) |
|            1. Initial mesh            |         2. After 1 iteration          |         3. After n iterations         |
| ![polygirl_1](results/polygirl_4.png) | ![polygirl_1](results/polygirl_5.png) | ![polygirl_1](results/polygirl_6.png) |
|           4. After surgery            |          5. After clustering          |    6. Comparison with initial mesh    |

**Analysis:** The result looks like a real human skeleton, but there are two problems:
 * the Laplacian could not reach an area of 0 (see the basin and the head)
 * the arms of the skeleton are lower than the real arms, the shoulders were attracted in the direction of the basin

### Robot

|                                 |                                 |                                 |
| :-----------------------------: | :-----------------------------: | :-----------------------------: |
| ![robot_1](results/robot_1.png) | ![robot_1](results/robot_2.png) | ![robot_1](results/robot_3.png) |
|         1. Initial mesh         |      2. After 1 iteration       |      3. After n iterations      |
| ![robot_1](results/robot_4.png) | ![robot_1](results/robot_5.png) | ![robot_1](results/robot_6.png) |
|        4. After surgery         |       5. After clustering       | 6. Comparison with initial mesh |

**Analysis:** This is a good illustration of the fact that our algorithm does not work for objects made from multiple independent meshes (wrong topology). Since the parts of the robot were not connected, the resulting skeleton is incorrect.

### Temple

|                                   |                                   |                                   |
| :-------------------------------: | :-------------------------------: | :-------------------------------: |
| ![temple_1](results/temple_1.png) | ![temple_1](results/temple_2.png) | ![temple_1](results/temple_3.png) |
|          1. Initial mesh          |       2. After 1 iteration        |       3. After n iterations       |
| ![temple_1](results/temple_4.png) | ![temple_1](results/temple_5.png) | ![temple_1](results/temple_6.png) |
|         4. After surgery          |        5. After clustering        |  6. Comparison with initial mesh  |

**Analysis:** Same problem than with the robot: the parts of the temple were not connected, so the Laplacian of the roof gave a single line (see image 2). The method does not work for disconnected objects.
